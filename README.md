# terraform-provider-gandi

[gh: terraform-provider-gandi](https://github.com/go-gandi/terraform-provider-gandi)

You can find the binary of **terraform provider for gandi**
from here: [terraform-provider-gandi](https://gitlab.com/terraform-provider/terraform-provider-gandi/-/jobs/artifacts/main/raw/terraform-provider-gandi/terraform-provider-gandi?job=run-build)

## Install

```bash
PLUGIN="go-gandi/gandi"
VERSION=v2.2.4
PROJECT="terraform-provider-gandi"

mkdir -p ~/.local/share/terraform/plugins/registry.terraform.io/${PROJECT}/${VERSION}/linux_amd64/
cd       ~/.local/share/terraform/plugins/registry.terraform.io/${PROJECT}/${VERSION}/linux_amd64/

curl -sLo ${PROJECT} https://gitlab.com/terraform-provider/${PROJECT}/-/jobs/artifacts/main/raw/${PROJECT}/${PROJECT}?job=run-build

chmod +x ${PROJECT}

```
